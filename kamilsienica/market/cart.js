///PRODUCTS IN CART

var buyedProducts = [
  {
    id: "224",
    name: "Marchew",
    prince: 836,
    description:
      "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Deserunt, voluptatibus.",
    promotion: false,
    discount: 0.5,
    tax: 8,
    data_added: new Date(),
    category: ["vegetables"],
  },
  {
    id: "225",
    name: "Rzodkiewka",
    prince: 735,
    description:
      "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Deserunt, voluptatibus.",
    promotion: true,
    discount: 0.1,
    tax: 8,
    data_added: new Date(),
    category: ["vegetables"],
  },
  {
    id: "226",
    name: "Ser edamski",
    prince: 2253,
    description:
      "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Deserunt, voluptatibus.",
    promotion: true,
    tax: 23,
    data_added: new Date(),
    category: ["dairy"],
  },
  {
    id: "227",
    name: "Mleko",
    prince: 1152,
    description:
      "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Deserunt, voluptatibus.",
    promotion: true,
    tax: 23,
    data_added: new Date(),
    category: ["dairy"],
  },
  {
    id: "228",
    name: "Ketchup",
    prince: 589,
    description:
      "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Deserunt, voluptatibus.",
    promotion: false,
    tax: 23,
    data_added: new Date(),
    category: ["spices"],
  },
];

function rednerCartToHtml(name, price, desc, id, amout) {
  var item = `<div class="list-group-item d-flex bg-secondary text-white" id=${
    "item-" + id
  } >
    <div class="d-flex flex-column flex-fill">
      <h3>${name}</h3>
      <p>${desc} </p>
    </div>
    <div class="d-flex flex-column p-1">
      <button 
        id=${id}
        type="button"
        class="align-self-end btn-close btn-outline-danger"
        aria-label="Close"
      ></button>
      <div class="fw-bold mt-2 text-nowrap">${price.toFixed(2)} PLN</div>
      <div class="align-self-end"><h6>Ilość: ${amout}</h6></div>
    </div>
  </div>`;

  return item;
}

function getCartInfo(products) {
  var total = 0;
  var element = document.getElementById("cart");
  element.innerHTML = "";

  function removeElementClosure(id) {
    return function () {
      removeElement(id);
    };
  }

  for (let product of products) {
    total += calculateProductPrince(product);

    element.insertAdjacentHTML(
      "beforeend",
      rednerCartToHtml(
        product.name,
        calculateProductPrince(product),
        getCategoryDescription(product),
        product.id,
        "undefined" === typeof product.amout
          ? (product.amout = 1)
          : product.amout
      )
    );
    document
      .getElementById(product.id)
      .addEventListener("click", removeElementClosure(product.id), false);
  }

  total_price.innerHTML = "Suma: " + total.toFixed(2);
}

function removeElement(id) {
  for (var k = 0; k < buyedProducts.length; k++) {
    if (buyedProducts[k].id === id) {
      buyedProducts.splice(k, 1);
      break;
    }
  }
  getCartInfo(buyedProducts);
}

function addElementToCart(product_id) {
  var productExist = false;

  for (let buyed of buyedProducts) {
    if (buyed.id === product_id) {
      buyed.amout === undefined ? (buyed.amout = 2) : buyed.amout++;
      productExist = true;
      break
    }
  }

  if (!productExist) buyedProducts.push(getProductById(product_id));

  getCartInfo(buyedProducts);
}

function getProductById(product_id) {
  for (let product of products) {
    if (product.id === product_id) return product;
  }
}

getCartInfo(buyedProducts);
