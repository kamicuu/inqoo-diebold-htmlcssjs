// console.log("Hello world");

// var product = {};

// product.id = "123";
// product.name = "Banan";
// product.prince = 1410;
// product.description =
//   "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Deserunt, voluptatibus.";
// product.promotion = true;
// product.tax = 23;
// product.data_added = new Date();
// product.category = ["fruits"];

// var product3 = {
//   id: "125",
//   name: "Ziemniaki",
//   prince: 935,
//   description:
//     "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Deserunt, voluptatibus.",
//   promotion: false,
//   tax: 8,
//   data_added: new Date(),
//   category: ["vegetables"],
// };

// var product4 = Object.assign({}, product3);
// product4.name = "Pomidory";

// var product5 = {
//     ...product2,
//     rating: {...product2.rating},
//     id: '356',
//     prince: 2013,
//     name: 'chocolate cookies'
// }
//
// var product6 = JSON.parse(JSON.stringify(product2))

///PRODUCTS AVAILABLE TO BUY
var products = [
  {
    id: "124",
    name: "Placki",
    prince: 1010,
    description:
      "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Deserunt, voluptatibus.",
    promotion: false,
    discount: 0.3,
    tax: 23,
    data_added: new Date(),
    category: ["pancakes"],
  },
  {
    id: "125",
    name: "Ziemniaki",
    prince: 935,
    description:
      "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Deserunt, voluptatibus.",
    promotion: true,
    discount: 0.5,
    tax: 8,
    data_added: new Date(),
    category: ["vegetables"],
  },
  {
    id: "126",
    name: "Banana",
    prince: 1253,
    description:
      "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Deserunt, voluptatibus.",
    promotion: true,
    tax: 8,
    data_added: new Date(),
    category: ["fruits"],
  },
  {
    id: "127",
    name: "Orange",
    prince: 1621,
    description:
      "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Deserunt, voluptatibus.",
    promotion: true,
    tax: 8,
    data_added: new Date(),
    category: ["fruits"],
  },
];

var shop_discount = 0.3;
var filter_promoted = true;
var limit = 5;
var refreshBtn = document.getElementById("refresh_btn");

// var filer_promoted = true;
// var limit = 2;

// var index = 0;
// var count = 1;

// while ((product = products[index])) {
//   index++;

//   if (product.promotion == false && filer_promoted) continue;

//   if (count++ > limit) break;

//   console.log(
//     product.name +
//       " - " +
//       product.description +
//       " - " +
//       (product.promotion ? "PROMOCJA" : "")
//   );
// }

function renderToHTML(name, price, desc) {
  var item = `<div class="list-group-item d-flex">
    <div class="d-flex flex-column flex-fill">
        <h3>${name}</h3>
        <p>${desc}</p>
    </div>
    <div class="d-flex flex-column p-1 align-self-top">
        <div class="fw-bold">${price.toFixed(2)} PLN</div>
        <button class="btn btn-dark text-nowrap">Add to cart</button>
    </div>
    </div>`;

  return item;
}

refreshBtn.onclick = function () {
  getProductInfo(products);
};

getProductInfo(products);

function getProductInfo(products) {
  product_list.innerHTML = "";
  var count = 1;

  for (product of products) {
    if (count++ > limit) break;

    var desc = getCategoryDescription(product);

    product_list.innerHTML += renderToHTML(
      product.name,
      calculateProductPrince(product),
      desc
    );
  }
}

function calculateProductPrince(product) {
  var price = product.prince;

  if ("undefined" !== typeof product.amout) {
    price = product.prince * product.amout;
  }

  if (product.promotion) {
    var prince_after_discount = getValWithDiscount(product, price);
    var prince_with_tax = getValueWithTax(prince_after_discount, product);
    return prince_with_tax;
  } else {
    var prince_with_tax = getValueWithTax(price, product);
    return prince_with_tax;
  }
}

function getValWithDiscount(product, price) {
  if ("undefined" !== typeof product.discount)
    return price * (1 - product.discount);
  else return price * (1 - shop_discount);
}

function getValueWithTax(prince_after_discount, product) {
  return (
    ((prince_after_discount * product.tax) / 100 + prince_after_discount) / 100
  );
}

function getCategoryDescription(product) {
  var description = product.description;
  switch (product.category[0]) {
    case "pancakes":
      description += " Swietne placki";
    case "vegetables":
      description += " Super";
      break;
    default:
      description += " Normalny";
  }
  return description;
}
// var price_with_tax = (
//   ((product.prince * product.tax) / 100 + product.prince) /
//   100
// ).toFixed(2);
// var promo_text = product.promotion ? "PROMO" : "";
// console.log(
//   product.name +
//     " - " +
//     product.description +
//     " - " +
//     price_with_tax +
//     " - " +
//     promo_text +
//     " - " +
//     product.data_added.toLocaleDateString()
// );

// var price_with_tax2 = (
//   ((product2.prince * product2.tax) / 100 + product2.prince) /
//   100
// ).toFixed(2);
// var promo_text2 = product2.promotion ? "PROMO" : "";
// console.log(
//   product2.name +
//     " - " +
//     product2.description +
//     " - " +
//     price_with_tax2 +
//     " - " +
//     promo_text2 +
//     " - " +
//     product2.data_added.toLocaleDateString()
// );

// var price_with_tax3 = (
//   ((product3.prince * product3.tax) / 100 + product3.prince) /
//   100
// ).toFixed(2);
// var promo_text3 = product3.promotion ? "PROMO" : "";
// console.log(
//   product3.name +
//     " - " +
//     product3.description +
//     " - " +
//     price_with_tax3 +
//     " - " +
//     promo_text3 +
//     " - " +
//     product3.data_added.toLocaleDateString()
// );
