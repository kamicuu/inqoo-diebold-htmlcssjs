var paymentArray = []

function calculateInstallment(){
    payment = {}
    paymentArray = []

    payment.interest;
    payment.installmment;
    payment.duration = document.getElementById("loan_months").value;
    payment.sum = (document.getElementById("loan_amount").value)*100
    payment.capital = payment.sum/payment.duration;
    payment.loan_interest = document.getElementById("loan_interest").value;
    htmlObject = ``;


    for(let i = 1; i<=payment.duration; i++){
    

        payment.interest = (payment.sum*(payment.loan_interest/100))/12
        payment.sum = payment.sum-payment.capital; 
        payment.installmment = payment.capital+payment.interest;

        paymentArray.push({
            capital: payment.capital,
            interest: payment.interest,
            sum: payment.sum,
            installmment: payment.installmment

        })
     

    }

    renderTable(paymentArray)

}


function renderTable(paymentArray){

    var tbody = document.getElementById('js-calculate-credit');
    tbody.innerHTML = ``
    var month = 1;

    for(payment of paymentArray){

    var tr = document.createElement('tr')
    tr.innerHTML =`
        <td>${month++}</td>
        <td>${(payment.capital/100).toFixed(2)}</td>
        <td>${(payment.interest/100).toFixed(2)}</td>
        <td>${(payment.sum/100).toFixed(2)}</td>
        <td>${(payment.installmment/100).toFixed(2)}</td>`

        tbody.appendChild(tr)
  
    }
    
}

document.getElementById("js-count").onclick = calculateInstallment;
