class Products {
  constructor(products, selector) {
    (this._products = products),
      (this._selector = selector),
      (this._result = this._products);
  }

  filterProductByName(name) {
    loadJSON("http://127.0.0.1:3000/products").then((resp) => {
      this._products = resp;

      this._result = this._products.filter((item) =>
        item.name.toLocaleLowerCase().includes(name)
      );
      this.renderProductList();
    });
  }

  renderProductList() {
    var listEl = document.querySelector(this._selector);
    listEl.innerHTML = "";

    this._result.forEach((item) => {
      var htmlProductElement = document.createElement("div");
      htmlProductElement.innerHTML = `<div class="list-group-item d-flex" data-item-id=${
        item.id
      }>
            <div class="d-flex flex-column flex-fill">
                <h3>${item.name}</h3>
            </div>
            <div class="d-flex flex-column p-1 align-self-top">
                <div class="fw-bold">${item.price / 100} PLN</div>
            </div>
            </div>`;

      listEl.appendChild(htmlProductElement);

    });

  }

  toggleActivateProduct() {
    let target;

    document
      .querySelector(this._selector)
      .addEventListener("click", (event) => {
        target = event.target.closest("[data-item-id]");

        document
          .querySelectorAll("[data-item-id]")
          .forEach((ele) => ele.classList.remove("active"));

        target.classList.add("active");

        loadJSON(
          "http://127.0.0.1:3000/products/" + target.dataset.itemId
        ).then((resp) => {
          app.productsEditor.setData(resp);
        });

        // app.productsEditor.setData(
        //   this._result.find((item) => item.id === target.dataset.itemId)
        // );
      });
  }

}
