class ProductsEditorViev {
  constructor(selector) {
    this.el = document.querySelector(selector);
    this.form = this.el.querySelector("form");

    this.form.addEventListener("submit", (event) => {
      event.preventDefault();

      this._formData = app.productsEditor.getData();

      fetch("http://localhost:3000/products/" + this._formData.id, {
        method: "PUT",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(this._formData),
      })
        .then((res) => res.json)
        .then(productsInstance.filterProductByName(""))
    });
  }

  setData(data) {
    this._data = data;
    this.form.elements["name"].value = this._data["name"];
    this.form.elements["price"].value = this._data["price"] / 100;
  }

  getData() {
    const f = new FormData(this.form);
    const data = Object.fromEntries(f.entries());

    this._data["name"] = this.form.elements["name"].value;
    this._data["price_nett"] = parseFloat(this.form.elements["price"].value) * 100;

    return this._data;
  }
}
