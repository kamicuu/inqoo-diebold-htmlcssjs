/**
 * @typedef Task
 * @property {string} id
 * @property {string} title
 * @property {boolean} completed
 */

// class MyEventTarget {
//   listeners = {}
//   dispatchEvent(event) {
//     this.listeners[event.type]?.forEach(fn => fn(event))
//   }

//   addEventListener(type, fn) {
//     this.listeners[type] = this.listeners[type] || []
//     this.listeners[type].push(fn)
//   }
// }


export class CollectionEvent extends window.Event {
  /** @type {TasksCollection} */ currentTarget
  constructor(type, item) {
    super(type)
    this.data = item
  }
}
export class Collection extends window.EventTarget { }

export class TasksCollection extends Collection {

  /**
   * 
   * @param {Task[]} data 
   */
  constructor(data) {
    super()
    this._data = data
  }

  /** @type Task[] */
  _data = []


  /**
   * @param {Task[]} data 
   */
  reset(data) {
    this._data = data
    this.dispatchEvent(new CollectionEvent('reset'))
  }

  async fetch() {
    const res = await fetch('http://localhost:3000/tasks')
    const data = await res.json()
    this.reset(data)
  }

  save(task) {


    this.dispatchEvent(new CollectionEvent('save', task))
  }

  /** 
   * 
   * @returns {Task[]} tasks 
   */
  getAll() {
    return this._data
  }

  getById() { }

}
